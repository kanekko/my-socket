package mx.nebulab.mysocket;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class MainActivity extends AppCompatActivity {

    EditText editTextIpAddress, editTextPort;
    Button buttonConnect;
    TextView textViewResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextIpAddress = (EditText) findViewById(R.id.ed_txt_ip_address);
        editTextPort = (EditText) findViewById(R.id.ed_txt_port);
        buttonConnect = (Button) findViewById(R.id.btn_connect);
        textViewResponse = (TextView) findViewById(R.id.txt_view_response);


        buttonConnect.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Client myClient = new Client(editTextIpAddress.getText().toString(),
                                             Integer.parseInt(editTextPort.getText().toString()),
                                             textViewResponse);
                myClient.execute();
            }

        });

    }

}
