import java.io.*;
import java.net.*;

public class Server {

    public static void main(String[] args) throws IOException {

        ServerSocket serverSocket = new ServerSocket(3333);
        System.out.println("Waiting for clients to connect...");

        while (true) {
            Socket _socket = serverSocket.accept();
            InetAddress client = _socket.getInetAddress();
            System.out.println("Incoming connection from: " + client.getHostName() + "[" + client.getHostAddress() + "]");

            _socket.close();
        }
    }

}